package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JuricverTest {

    @Test
    public void testFactorial5(){
        Juricver juric = new Juricver();

        assertEquals(120, juric.factorial(5));
    }

    @Test
    public void testFactorial0(){
        Juricver juric = new Juricver();

        assertEquals(1, juric.factorial(0));
    }

    @Test
    public void testFactorialNegative(){
        Juricver juric = new Juricver();

        assertThrows(IllegalArgumentException.class, () -> juric.factorial(-10));
    }

}