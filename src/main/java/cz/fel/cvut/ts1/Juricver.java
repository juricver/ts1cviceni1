package cz.fel.cvut.ts1;

public class Juricver {

    public long factorial(int n){
        if(n<0){
            throw new IllegalArgumentException("ne zaporna hodnota");
        }
        if(n == 0){
            return 1;
        }
        return n*factorial(n-1);
    }
}
